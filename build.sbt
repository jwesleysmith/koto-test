
organization in ThisBuild := "io.koto"

name := "koto-test"

version in ThisBuild := "1.13"

scalaVersion in ThisBuild := "2.10.2"

resolvers in ThisBuild ++= Seq(
  Classpaths.typesafeResolver
, "mvn-local"         at "file://" + Path.userHome.absolutePath + "/.m2/repository"
, "a-public"          at "https://maven.atlassian.com/content/repositories/public/"
)

libraryDependencies in ThisBuild ++= Seq(
  "io.kadai"                          %% "kadai"          % "1.0.0"
, "org.specs2"                        %% "specs2"         % "2.1.1" % "test"
, "junit"                              % "junit"          % "4.10"  % "test"
)

scalacOptions in ThisBuild ++= Seq("-deprecation", "-unchecked", "-feature", "-language:_")

fork in Test := true

javaOptions in test += "-Xmx2G"
