package koto
package data

import scalaz._
import Scalaz._
import scala.concurrent.Future

trait StorageAPI {
  def get[A](hash: Hash, f: Content => A): Storage[A] =
    StorageOp.get(hash, f).lift

  def set[A](content: Content, f: Hash => A): Storage[A] =
    StorageOp.set(content, f).lift

  private[data] abstract class Interpreter[R[_]] {
    def apply[A](op: StorageOp[A]): R[A]
  }
}

/**
 * A pure DSL for interacting with the Database.
 *
 * see: https://github.com/NICTA/rng/blob/master/src/main/scala/com/nicta/rng/Rng.scala
 *
 * for the inspiration
 */
sealed abstract class Storage[+A] { //TODO make A invariant when we move to scalaz 7.1
  import Storage._, StorageOp._

  def free: Free[StorageOp, A]

  def map[B](f: A => B): Storage[B] =
    Storage(free map f)

  def flatMap[B](f: A => Storage[B]): Storage[B] =
    Storage(free flatMap (f(_).free))

  def ap[X](f: Storage[A => X]): Storage[X] =
    for {
      ff <- f
      aa <- this
    } yield ff(aa)

  def zip[X](q: Storage[X]): Storage[(A, X)] =
    zipWith(q)(a => (a, _))

  def zipWith[B, C](r: Storage[B])(f: A => B => C): Storage[C] =
    r.ap(map(f))

  def foldRun[B, AA >: A](b: B)(f: (B, StorageOp[Storage[AA]]) => (B, Storage[AA])): (B, AA) =
    free.foldRun[B, AA](b) { (bb, t) =>
      f(bb, t map {
        a => Storage(a)
      }) :-> { _.free }
    }

  private[data] def resume: StorageResume[A] =
    free.resume match {
      case -\/(x) => StorageCont(x map Storage.apply)
      case \/-(x) => StorageTerm(x)
    }
}

/**
 * Verbs for dealing with a Storage.
 */
object Storage extends StorageAPI {
  import StorageOp._

  type FreeStorageOp[+A] = Free[StorageOp, A] //TODO make A invariant when we move to scalaz 7.1

  private[data] def apply[A](f: Free[StorageOp, A]): Storage[A] =
    new Storage[A] {
      val free = f
    }

  implicit object StorageMonad extends Monad[Storage] {
    def point[A](a: => A): Storage[A] =
      new Storage[A] {
        val free: Free[StorageOp, A] = a.point[FreeStorageOp]
      }

    def bind[A, B](db: Storage[A])(f: A => Storage[B]): Storage[B] =
      db flatMap f

    override def map[A, B](db: Storage[A])(f: A => B): Storage[B] =
      db map f
  }
}
