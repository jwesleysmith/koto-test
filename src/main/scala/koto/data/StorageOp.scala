package koto
package data

import scala.concurrent.Future
import scalaz.{ Applicative, Foldable, Free, Functor, syntax }, Free.{ Return, Suspend }
import scalaz.Scalaz._
import syntax.applicative._

object StorageOp {

  def get[F[_], A](hash: Hash, f: Content => A): StorageOp[A] =
    Get(hash, f)

  def set[F[_], A](content: Content, f: Hash => A): StorageOp[A] =
    Set(content, f)

  def point[F[_], A](a: A): StorageOp[A] =
    Point(a)

  implicit object StorageOpFunctor extends Functor[StorageOp] {
    override def map[A, B](op: StorageOp[A])(f: A => B): StorageOp[B] =
      op map f
  }
}

sealed abstract class StorageOp[+A] {
  import StorageOp._

  type F[_]

  def lift: Storage[A] =
    Storage { Suspend { map { Return(_) } } }

  def map[B](fa: A => B): StorageOp[B] =
    this match {
      case get @ Get(hash, f)    => Get[get.F, B](hash, f andThen fa)
      case set @ Set(content, f) => Set[set.F, B](content, f andThen fa)
      case Point(p)              => Point(fa(p))
    }
}

case class Point[A](a: A) extends StorageOp[A] { type F[_] = Id[_] }

case class Get[FF[_], A](hash: Hash, f: FF[Array[Byte]] => A) extends StorageOp[A] {
  type F[_] = FF[_]
}

case class Set[FF[_], A](content: Content, f: Hash => A) extends StorageOp[A] {
  type F[_] = FF[_]
}
