package koto

import scalaz._
import Scalaz._

package object data {
  type File = java.io.File

  sealed trait SHA1
  type Hash = String @@ SHA1
  object Hash {
    def apply(s: String): Hash = Tag(s)
  }

  type Content = Stream[Array[Byte]]
  
  object File extends (String => File) {
    def apply(s: String) =
      new java.io.File(s)

    def from(p: File, s: String) =
      new java.io.File(p, s)
  }
}