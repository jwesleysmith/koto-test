package koto.data

import scalaz._, Scalaz._, Free._

private[data] sealed abstract class StorageResume[+A] {
  def map[B](f: A => B): StorageResume[B] =
    this match {
      case StorageCont(x) => StorageCont(x map { _ map f })
      case StorageTerm(x) => StorageTerm(f(x))
    }

  def free: Storage[A] =
    Storage {
      this match {
        case StorageCont(x) => Suspend(x map { _.free })
        case StorageTerm(x) => Return(x)
      }
    }

  def term: Option[A] =
    this match {
      case StorageCont(_) => None
      case StorageTerm(x) => Some(x)
    }

  def cont: Option[StorageOp[Storage[A]]] =
    this match {
      case StorageCont(x) => Some(x)
      case StorageTerm(x) => None
    }
}

/** Continue in the Free */
private[data] case class StorageCont[+A](op: StorageOp[Storage[A]]) extends StorageResume[A]
/** Terminate and return a */
private[data] case class StorageTerm[+A](a: A) extends StorageResume[A]

private[data] object StorageResume {

  implicit object StorageResumeFunctor extends Functor[StorageResume] {
    def map[A, B](fa: StorageResume[A])(f: A => B) =
      fa map f
  }

  def distribute[G[_], A](a: StorageResume[G[A]])(implicit D: Distributive[G]): G[StorageResume[A]] =
    D.cosequence(a)

  def distributeR[A, B](a: StorageResume[A => B]): A => StorageResume[B] =
    distribute[({ type f[x] = A => x })#f, B](a)

  def distributeRK[A, B](a: StorageResume[A => B]): Kleisli[StorageResume, A, B] =
    Kleisli(distributeR(a))

  def distributeK[F[+_]: Distributive, A, B](a: StorageResume[Kleisli[F, A, B]]): Kleisli[F, A, StorageResume[B]] =
    distribute[({ type f[x] = Kleisli[F, A, x] })#f, B](a)
}
